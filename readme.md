# Dad Simulator

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)

## About the Game

Find way to the street


## Gameplay

Walking around home, speak with famaly members, hit things

## Features

Key features of game:
Funny graphycs

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/dad-simulator.git`
2. Navigate to the game directory: `cd dad-simulator`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only webGL available

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
