﻿using System;
using System.Collections;
using Core.Constrains;
using Mechanics;
using Mechanics.Environment.Abstractions;
using Mechanics.Player;
using UnityEngine;

namespace LiftStuff
{
    public enum LiftPosition
    {
        Down,
        Up
    }

    public class Lift : InteractableObject
    {
        [SerializeField] private float speed;
        [SerializeField] private Transform upPoint;
        [SerializeField] private Transform downPoint;
        [SerializeField] private Transform liftTransform;

        [SerializeField] private LiftPosition currentPos;
        private bool _isPlayerInside;

        private GameObject _player;
        private MovementSystem _playerMovement;
        private ActionsSystem _actionsSystem;
        private static readonly int Move = Animator.StringToHash("Walk");

        protected override void Start()
        {
            base.Start();

            _player = GameObject.FindWithTag(Tags.Player);
            _playerMovement = _player.GetComponent<MovementSystem>();
            _actionsSystem = _player.GetComponent<ActionsSystem>();
        }

        private IEnumerator LiftIE()
        {
            float targetPointY;
            if (currentPos is LiftPosition.Down)
            {
                currentPos = LiftPosition.Up;
                targetPointY = upPoint.position.y;
                CameraFollow.FloorNumber = 1;
            }
            else
            {
                currentPos = LiftPosition.Down;
                targetPointY = downPoint.position.y;
                CameraFollow.FloorNumber = 0;
            }
            while (Math.Abs(liftTransform.position.y - targetPointY) > 0.1f)
            {
                var position = liftTransform.position;
                position = Vector3.Lerp(position,
                    new Vector3(position.x, targetPointY, position.z),
                    speed * Time.deltaTime);
                liftTransform.position = position;
                _player.transform.position = position;
                yield return null;
            }

            Debug.Log("here");

            StartCoroutine(MovePlayerIE());
        }

        protected override void OnInteract()
        {
            StartCoroutine(MovePlayerIE());
        }

        private IEnumerator MovePlayerIE()
        {
            _isPlayerInside = !_isPlayerInside;
            Debug.Log("_isPlayerInside = " + _isPlayerInside);
            var target = FindTarget(currentPos is LiftPosition.Down ? downPoint.position : upPoint.position);


            _actionsSystem.PlayerAnimator.SetBool(Move, true);
            _playerMovement.Flip(target);
            InputManager.Target = target;

            while (Vector3.Distance(_player.transform.position, target) >= 0.1f)
            {
                _playerMovement.Move(target);
                yield return null;
            }

            _actionsSystem.PlayerAnimator.SetBool(Move, false);

            if (_isPlayerInside)
                StartCoroutine(LiftIE());
        }

        private Vector3 FindTarget(Vector3 pointPosition)
        {
            Vector3 position = _player.transform.position;
            var target = !_isPlayerInside
                ? new Vector3(pointPosition.x, position.y,
                    pointPosition.z) - Vector3.left * 10f
                : new Vector3(pointPosition.x, position.y,
                    pointPosition.z);
            return target;
        }
    }
}