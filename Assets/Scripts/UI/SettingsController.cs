﻿using Core.Constrains;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SettingsController : MonoBehaviour
    {
        [SerializeField] private Slider soundSlider;
        [SerializeField] private Button langRu;
        [SerializeField] private Button langEn;


        private void Start()
        {
            SetLanguage(PlayerPrefs.GetString(PrefKeys.Language, "en"));
            OnSoundSliderChange(PlayerPrefs.GetFloat(PrefKeys.Sound, 1f));

            soundSlider.onValueChanged.AddListener(OnSoundSliderChange);
            langRu.onClick.AddListener(() => SetLanguage("ru"));
            langEn.onClick.AddListener(() => SetLanguage("en"));
        }

        private void SetLanguage(string languageCode)
        {
            PlayerPrefs.SetString(PrefKeys.Language, languageCode);
        }

        private void OnSoundSliderChange(float newValue)
        {
            PlayerPrefs.SetFloat(PrefKeys.Sound, newValue);
        }
    }
}