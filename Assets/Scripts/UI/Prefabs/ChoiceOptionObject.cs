﻿using Core.Dialogues;
using UI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Prefabs
{
    public class ChoiceOptionObject : MonoBehaviour
    {
        [SerializeField] private Text textField;
        [SerializeField] private Button button;

        private DialoguePopup _dialoguePopup;
        private ChoiceOption _choiceOption;

        
        public void DisplayOption(ChoiceOption choiceOption, DialoguePopup dialoguePopup)
        {
            _choiceOption = choiceOption;
            _dialoguePopup = dialoguePopup;
            button.onClick.AddListener(OnOptionSelected);
            textField.text = choiceOption.Text;
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(OnOptionSelected);
        }


        private void OnOptionSelected() => _dialoguePopup.MakeChoice(_choiceOption);
    }
}