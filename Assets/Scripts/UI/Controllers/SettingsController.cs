﻿using Core.LocalizationTool;
using UI.Popups;
using UnityEngine;

namespace UI.Controllers
{
    public class SettingsController : MonoBehaviour
    {
        public void SetGameLanguage(string langCode)
        {
            if (string.IsNullOrWhiteSpace(langCode) || langCode.Length != 2)
                InfoPopup.Show("oops", $"Language with code {langCode} not found :(");
            else
                Localization.SetLanguage(langCode);
        }
    }
}