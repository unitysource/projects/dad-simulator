﻿using Core.Dialogues;
using Core.LocalizationTool;
using Mechanics;
using Mechanics.Player;
using UI.Prefabs;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popups
{
    public class DialoguePopup : MonoBehaviour
    {
        [SerializeField] private Button nextButton;

        [SerializeField] private Transform choiceContainer;
        [SerializeField] private ChoiceOptionObject choiceOptionPrefab;

        [SerializeField] private Text replicaTextField;

        [SerializeField] [Space] private Image speakerAvatarImage;
        [SerializeField] private Text speakerNameField;


        private Dialogue _currentDialogue;
        private DialogueSystem _dialogueSystem;
        private bool onChoiceNow;


        private void OnEnable()
        {
            InputManager.EnterNext += NextReplica;
            nextButton.onClick.AddListener(NextReplica);
        }

        private void OnDisable()
        {
            InputManager.EnterNext -= NextReplica;
            nextButton.onClick.RemoveListener(NextReplica);
        }


        public void StartDialogue(string dialogueName, DialogueSystem dialogueSystem)
        {
            _currentDialogue = DialoguesManager.LoadDialogue(dialogueName, dialogueSystem);
            _dialogueSystem = dialogueSystem;
            ShowPopup();
            NextReplica();
        }

        public void FinishDialogue()
        {
            HidePopup();
            DisplayText(string.Empty);
            _dialogueSystem.OnDialogueFinish();
        }

        public void NextReplica()
        {
            _currentDialogue.Replicas.MoveNext();

            if (_currentDialogue.Replicas.Current is null)
            {
                if (onChoiceNow) return;

                onChoiceNow = true;
                HidePopup();
                DisplayChoice(_currentDialogue.FinalChoice);
            }
            else
            {
                DisplayReplica(_currentDialogue.Replicas.Current);
            }
        }

        public void MakeChoice(ChoiceOption selectedOption)
        {
            onChoiceNow = false;

            choiceContainer.gameObject.SetActive(true);

            for (int i = 0; i < choiceContainer.childCount; i++)
                Destroy(choiceContainer.GetChild(i).gameObject);
            
            selectedOption.Apply(_dialogueSystem);

            if (selectedOption.NextDialogue is null)
                FinishDialogue();

            _currentDialogue = selectedOption.NextDialogue;

            if (_currentDialogue != null)
            {
                ShowPopup();
                NextReplica();
            }
        }

        private void DisplayReplica(Replica replica)
        {
            DisplayText(replica.Text);
            DisplaySpeakerData(replica.Speaker);
        }

        private void DisplayChoice(Choice choice)
        {
            ChoiceOptionObject prefab = choiceOptionPrefab;
            Transform container = choiceContainer;

            foreach (ChoiceOption choiceOption in choice.Options)
            {
                ChoiceOptionObject choiceInstance = Instantiate(prefab, container);
                choiceInstance.DisplayOption(choiceOption, this);
            }
        }

        private void ShowPopup()
        {
            gameObject.SetActive(true);
        }

        private void HidePopup()
        {
            gameObject.SetActive(false);
        }


        private void DisplayText(string text)
        {
            replicaTextField.text = text;
        }

        private void DisplaySpeakerData(Speaker speaker)
        {
            speakerNameField.color = speaker.Color;
            speakerNameField.text = speaker.LocalizedName.ToLocalized();
            speakerAvatarImage.sprite = speaker.Avatar;
        }
    }
}