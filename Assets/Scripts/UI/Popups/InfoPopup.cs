﻿using UnityEngine;

namespace UI.Popups
{
    public class InfoPopup
    {
        public static void Show(string title, string message, int time = 5)
        {
            Debug.Log($"{title}\n{message}");
        }
    }
}