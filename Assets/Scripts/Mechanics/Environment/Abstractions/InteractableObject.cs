﻿using System.Collections;
using Core;
using Core.Constrains;
using Mechanics.Player;
using UnityEngine;

namespace Mechanics.Environment.Abstractions
{
    public abstract class InteractableObject : MonoBehaviour
    {
        [Header("Activation mode")] [SerializeField]
        private bool hotkeyUsed;

        [SerializeField] private bool autoUsed;
        [SerializeField] private float autoUseActivationTime = 5f;

        [SerializeField] private AudioClip interactAudio;

        [Header("Tooltips")] [SerializeField] private bool showTooltip;
        [SerializeField] private Vector3 tipPosition = Vector3.up;


        private WaitForSeconds _delay;
        private bool _isPlayerInRange;
        private GameObject _tooltip;


        public bool IsPlayerInRange
        {
            get => _isPlayerInRange;
            set
            {
                if (showTooltip)
                {
                    if (value) OnPlayerEnter();
                    else OnPlayerExit();
                }

                _isPlayerInRange = value;
            }
        }

        protected abstract void OnInteract();

        protected virtual void Start()
        {
            if (autoUsed)
            {
                _delay = new WaitForSeconds(autoUseActivationTime);
            }
        }

        protected virtual void OnEnable()
        {
            if (hotkeyUsed) InputManager.SpaceInteract += HotKeyInteract;
        }

        protected virtual void OnDisable()
        {
            if (hotkeyUsed) InputManager.SpaceInteract -= HotKeyInteract;
        }

        protected void HotKeyInteract()
        {
            if (IsPlayerInRange) DoInteraction();
        }

        private void DoInteraction()
        {
            StatsController.InteractionsCount += 1;
            OnInteract();

            GameMaster.InteractionSource.clip = interactAudio ? interactAudio : GameMaster.DefaultAudio;
            GameMaster.InteractionSource.PlayOneShot(GameMaster.InteractionSource.clip);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag(Tags.Player)) return;
            // Debug.Log(other.name);
            IsPlayerInRange = true;
            if (autoUsed)
                StartCoroutine(WaitToInteractRoutine());
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.CompareTag(Tags.Player)) return;
            IsPlayerInRange = false;
        }


        private IEnumerator WaitToInteractRoutine()
        {
            yield return _delay;
            if (!IsPlayerInRange) yield break;
            DoInteraction();
        }

        protected virtual void OnPlayerEnter()
        {
            GameObject tip = ObjectsPool.ActivateToolTip();
            tip.transform.SetParent(transform);
            tip.SetActive(true);
            tip.transform.localPosition = tipPosition;
            _tooltip = tip;
        }

        protected virtual void OnPlayerExit()
        {
            if (_tooltip != null)
                ObjectsPool.StoreToolTip(_tooltip);
        }
    }
}