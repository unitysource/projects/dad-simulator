﻿using UnityEngine;

namespace Mechanics.Environment.Abstractions
{
    public abstract class UsingTarget : MonoBehaviour
    {
        public abstract void OnUse();
    }
}