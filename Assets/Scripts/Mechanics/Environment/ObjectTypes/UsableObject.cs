﻿using Mechanics.Environment.Abstractions;
using UnityEngine;

namespace Mechanics.Environment.ObjectTypes
{
    public class UsableObject : InteractableObject
    {
        [SerializeField] private UsingTarget target;

        protected override void OnInteract()
        {
            target.OnUse();
        }
    }
}