﻿using Core.Constrains;
using Mechanics.Environment.Abstractions;
using Mechanics.Player;
using UnityEngine;

namespace Mechanics.Environment.ObjectTypes
{
    public class TalkativeObject : InteractableObject
    {
        [Header("Dialogue")] [SerializeField] private bool commentHotkey = true;
        [SerializeField] private string dialogueName;
        [SerializeField] private bool disableAfterInteract;
        [SerializeField] private bool destroyAfterInteract;
        [SerializeField] private GameObject parent;
        


        private DialogueSystem _dialogueSystem;

        protected override void Start()
        {
            base.Start();
            GameObject player = GameObject.FindWithTag(Tags.Player);
            _dialogueSystem = player.GetComponent<DialogueSystem>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (commentHotkey) InputManager.Comment += HotKeyInteract;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (commentHotkey) InputManager.Comment -= HotKeyInteract;
        }


        protected override void OnInteract()
        {
            _dialogueSystem.OnDialogueStart(dialogueName, this);
        }

        public void OnFinish()
        {
            if (disableAfterInteract)
                parent.SetActive(false);
            else if (destroyAfterInteract) 
                Destroy(parent, 0.1f);
        }
    }
}