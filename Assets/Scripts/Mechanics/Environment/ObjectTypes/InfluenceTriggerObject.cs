﻿using System.Collections;
using Core.Constrains;
using Mechanics.Player;
using UnityEngine;

namespace Mechanics.Environment.ObjectTypes
{
    public class InfluenceTriggerObject : MonoBehaviour
    {
        [SerializeField] private short modifier;
        [SerializeField] private NeedStage needStage;
        [SerializeField] [Space] private float minActivationTime = 0.5f;
        [SerializeField] private float maxActivationTime = 2f;


        private bool _started;
        private NeedsSystem _needsSystem;

        private void Start()
        {
            _needsSystem = GameObject.FindWithTag(Tags.Player).GetComponent<NeedsSystem>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (!_started && other.CompareTag(Tags.Player))
                StartCoroutine(ApplyIE());
        }

        private IEnumerator ApplyIE()
        {
            _started = true;
            yield return new WaitForSeconds(Random.Range(minActivationTime, maxActivationTime));
            _needsSystem.UpdateByName(needStage, modifier);
            _started = false;
        }
    }
}