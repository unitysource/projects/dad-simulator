﻿using Core.Constrains;
using Mechanics.Environment.Abstractions;
using Mechanics.Player;
using UnityEngine;

namespace Mechanics.Environment.ObjectTypes
{
    [RequireComponent(typeof(Collider))]
    public class PortableObject : InteractableObject
    {
        [SerializeField] private GameObject parent;

        public Collider Collider { get; private set; }
        public GameObject Parent => parent;
        [HideInInspector] public Rigidbody rb;

        private InventorySystem _playerInventory;
 

        protected override void Start()
        {
            base.Start();
            rb = parent.GetComponent<Rigidbody>();
            Collider = GetComponent<Collider>();
            _playerInventory = GameObject.FindWithTag(Tags.Player).GetComponent<InventorySystem>();
        }

        protected override void OnInteract()
        {
            Debug.Log("here");
            _playerInventory.AddItem(this);
        }
    }
}