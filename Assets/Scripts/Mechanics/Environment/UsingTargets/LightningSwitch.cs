﻿using System.Collections;
using System.Collections.Generic;
using Mechanics.Environment.Abstractions;
using UnityEngine;

namespace Mechanics.Environment.UsingTargets
{
    public class LightningSwitch : UsingTarget
    {
        [SerializeField] private List<Light> lights;


        public override void OnUse()
        {
            foreach (Light roomLight in lights)
            {
                StartCoroutine(SwitchIE(roomLight));
            }
        }

        private IEnumerator SwitchIE(Light roomLight)
        {
            yield return roomLight;
        }
    }
}