﻿using UnityEngine;

namespace Mechanics.CrazyJobs
{
    public abstract class CrazyJob : MonoBehaviour
    {
        protected abstract void OnCrazyStarts(Crazy stage);
    }
}