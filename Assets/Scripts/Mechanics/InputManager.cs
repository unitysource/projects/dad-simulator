using System;
using Core.Constrains;
using Mechanics.Player;
using UnityEngine;

namespace Mechanics
{
    public class InputManager : MonoBehaviour
    {
        private Camera _camera;

        private GameObject _player;
        private MovementSystem _playerMovementSystem;
        private InventorySystem _inventorySystem;
        private ActionsSystem _actionsSystem;
        private static readonly int Move = Animator.StringToHash("Walk");

        public static event Action<Rigidbody, int> Throw;
        public static event Action SpaceInteract;
        public static event Action Comment;
        public static event Action EnterNext;
        public static event Action Exit;
        
        public static Vector3 Target { get; set; }
        public static bool CanInput { get; set; } = true;
        public static bool DialogueMode { get; set; }


        private void Start()
        {
            _camera = Camera.main;
            _player = GameObject.FindWithTag(Tags.Player);
            _playerMovementSystem = _player.GetComponent<MovementSystem>();
            _inventorySystem = _player.GetComponent<InventorySystem>();
            _actionsSystem = _player.GetComponent<ActionsSystem>();
            Target = _player.transform.position;
        }

        private void Update()
        {
            ExitAction();

            if (!CanInput) return;

            if (DialogueMode)
            {
                EnterNextAction();
            }
            else
            {
                PlayerMovement();
                ThrowAction();
                PickUpAction();
                CommentAction();
            }
        }

        private void PickUpAction()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                SpaceInteract?.Invoke();
        }

        private void CommentAction()
        {
            if (Input.GetKeyDown(KeyCode.C))
                Comment?.Invoke();
        }

        private void EnterNextAction()
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                EnterNext?.Invoke();
            }

        }

        private void ExitAction()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Exit?.Invoke();
        }

        private void ThrowAction()
        {
            Rigidbody objectRb = _inventorySystem.ObjectRb;
            if (Input.GetKeyDown(KeyCode.X) && objectRb != null)
            {
                int sign = _player.transform.localScale.x < 0 ? 1 : -1;
                Throw?.Invoke(objectRb, sign);
                _inventorySystem.ObjectRb = null;
            }
        }

        private void PlayerMovement()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out RaycastHit hit, 50f, LayerMask.GetMask(Layers.Ground)))
                {
                    Target = hit.point;
                    _playerMovementSystem.Flip(Target);
                    _actionsSystem.PlayerAnimator.SetBool(Move, true);
                }
            }

            float distance = Vector3.Distance(_player.transform.position, Target);

            if (distance >= 0.1f)
            {
                _playerMovementSystem.Move(Target);
            }
            else if (distance < 0.1f && _actionsSystem.PlayerAnimator.GetBool(Move))
            {
                _playerMovementSystem.AudioSrc.Stop();
                _actionsSystem.PlayerAnimator.SetBool(Move, false);
            }
        }
    }
}