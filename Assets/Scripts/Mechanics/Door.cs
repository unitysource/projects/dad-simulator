﻿using System;
using System.Collections;
using Core.Constrains;
using Mechanics.Environment.Abstractions;
using Mechanics.Player;
using UnityEngine;

namespace Mechanics
{
    public enum DoorState
    {
        Open,
        Close
    }

    public class Door : InteractableObject
    {
        [SerializeField] private bool needKey;
        [SerializeField] private float rotateSpeed = 5f;
        [SerializeField] private float delay = 6f;
        [SerializeField] private Transform parent;
        [SerializeField] private Transform leftPoint;
        [SerializeField] private Transform rightPoint;

        private Collider _collider;

        private GameObject _player;
        private MovementSystem _playerMovement;
        private ActionsSystem _actionsSystem;
        private static readonly int Move = Animator.StringToHash("Walk");

        protected override void Start()
        {
            base.Start();
            _collider = GetComponent<Collider>();
            
            _player = GameObject.FindWithTag(Tags.Player);
            _playerMovement = _player.GetComponent<MovementSystem>();
            _actionsSystem = _player.GetComponent<ActionsSystem>();
        }

        // public void OpenDoor()
        // {
        //     if (needKey)
        //     {
        //         StartCoroutine(LerpRotateIE(DoorState.Open));
        //     }
        // }
        //
        // public void CLoseDoor()
        // {
        //     if (needKey)
        //     {
        //         StartCoroutine(LerpRotateIE(DoorState.Close));
        //     }
        // }

        private IEnumerator LerpRotateIE(DoorState state)
        {
            float yRot = state == DoorState.Open ? 90f : 0f;

            if (state == DoorState.Open)
            {
                InputManager.CanInput = false;
            }

            while (Mathf.Abs(parent.transform.localRotation.eulerAngles.y - yRot) > 2f)
            {
                var rotation = parent.transform.localRotation;
                rotation = Quaternion.RotateTowards(rotation, Quaternion.Euler(Vector3.up * yRot),
                    Time.deltaTime * rotateSpeed);
                parent.transform.localRotation = rotation;
                yield return null;
            }

            if (state == DoorState.Close)
            {
                yield return new WaitForSeconds(.1f);
                InputManager.CanInput = true;
            }
        }

        private IEnumerator OpenAndCloseDoor()
        {
            StartCoroutine(LerpRotateIE(DoorState.Open));
            yield return new WaitForSeconds(delay);
            StartCoroutine(LerpRotateIE(DoorState.Close));
        }

        protected override void OnInteract()
        {
            Debug.Log("here");
            StartCoroutine(OpenAndCloseDoor());
            StartCoroutine(MovePlayerIE());
        }

        private IEnumerator MovePlayerIE()
        {
            Vector3 target;
            
            if (_player.transform.position.x > parent.position.x)
                target = rightPoint.position;
            else
                target = leftPoint.position;

            _actionsSystem.PlayerAnimator.SetBool(Move, true);
            _playerMovement.Flip(target);
            InputManager.Target = target;

            while (Vector3.Distance(_player.transform.position, target) >= 0.1f)
            {
                _playerMovement.Move(target);
                yield return null;
            }

            _actionsSystem.PlayerAnimator.SetBool(Move, false);
        }
    }
}