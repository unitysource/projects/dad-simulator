﻿using System;
using System.Collections;
using Mechanics.Environment.Abstractions;
using UnityEngine;

namespace Mechanics
{
    public enum LightState
    {
        On,
        Off
    }
    
    public class LightSwitcher : InteractableObject
    {
        [SerializeField] private GameObject onModel;
        [SerializeField] private GameObject offModel;
        [SerializeField] private Light lightObj;
        [SerializeField] private float minLightIntensity;
        [SerializeField] private float maxLightIntensity;
        [SerializeField] private LightState currentState;
        // [SerializeField] private AnimationCurve curve;
        [SerializeField] private float speed=10f;

        private float _lightIntensity;

        protected override void Start()
        {
            base.Start();
            _lightIntensity = lightObj.intensity;
        }

        protected override void OnInteract()
        {
            Debug.Log("here");
            
            if (currentState is LightState.On)
            {
                onModel.SetActive(false);
                offModel.SetActive(true);
                currentState = LightState.Off;
            }
            else
            {
                onModel.SetActive(true);
                offModel.SetActive(false);
                currentState = LightState.On;
            }
            
            StartCoroutine(LerpSwitchIE(currentState));
        }
        
        private IEnumerator LerpSwitchIE(LightState state)
        {
            float intensity = state is LightState.On ? maxLightIntensity : minLightIntensity;
            
            while (Math.Abs(lightObj.intensity - intensity) > 0.01f)
            {
                var intensity1 = lightObj.intensity;
                // lightObj.intensity = Mathf.Lerp(intensity1, intensity, curve.Evaluate(intensity1) * Time.deltaTime);
                lightObj.intensity = Mathf.Lerp(intensity1, intensity, speed * Time.deltaTime);
                yield return null;
            }
        }
    }
}