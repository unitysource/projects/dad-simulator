﻿using UnityEngine;

namespace Mechanics.Player
{
    public class PlayerSystem : MonoBehaviour
    {
        public bool GameLost { get; private set; }


        public void LoseGame()
        {
            GameLost = true;
        }
    }
}