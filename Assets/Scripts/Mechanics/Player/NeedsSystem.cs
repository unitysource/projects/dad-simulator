﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Constrains;
using Mechanics.Needs;
using UnityEngine;

namespace Mechanics.Player
{
    [RequireComponent(typeof(CrazyModeSystem))]
    [RequireComponent(typeof(PlayerSystem))]
    public class NeedsSystem : MonoBehaviour
    {
        [SerializeField] private List<Needing> needs;
        [SerializeField] private float updateDeltaTime;

        private float TotalNeedsRelativeValue => needs.Sum(n => n.RelativeValue) / needs.Count;


        private CrazyModeSystem _crazyModeSystem;
        private PlayerSystem _playerSystem;
        private WaitForSeconds _delay;

        private void Start()
        {
            _crazyModeSystem = GetComponent<CrazyModeSystem>();
            _playerSystem = GetComponent<PlayerSystem>();
            _delay = new WaitForSeconds(updateDeltaTime);
            StartCoroutine(UpdateParamsIE());
        }


        public void UpdateByName(string needName, short value)
        {
            Needing need = needs.First(n => n.Name == needName);

            bool crazyBefore = need.Crazy;

            need.Value = value;

            if (need.Crazy && !crazyBefore)
                _crazyModeSystem.StageUp();

            if (!need.Crazy && crazyBefore)
                _crazyModeSystem.StageDown();
        }

        public void UpdateByName(NeedStage needStage, short value)
        {
            UpdateByName(needStage.ToString(), value);
        }

        private IEnumerator UpdateParamsIE()
        {
            while (!_playerSystem.GameLost)
            {
                yield return _delay;

                foreach (Needing needing in needs) 
                    needing.Update();
            }
        }
    }
}