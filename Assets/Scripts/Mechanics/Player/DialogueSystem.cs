﻿using System.Collections.Generic;
using Core.Dialogues;
using Mechanics.Environment.ObjectTypes;
using UnityEngine;

namespace Mechanics.Player
{
    [RequireComponent(typeof(NeedsSystem))]
    public class DialogueSystem : MonoBehaviour
    {
        private IDictionary<string, short> variables;

        private NeedsSystem _needsSystem;
        private TalkativeObject _talkativeObject;

        private void Start()
        {
            _needsSystem = GetComponent<NeedsSystem>();
            variables = new Dictionary<string, short>();
        }

        public int GetVariable(string key) => variables[key];

        public void UpdateVariable(string key, short delta)
        {
            Debug.Log("key = " + key);
            Debug.Log("delta = " + delta);
            if (variables.ContainsKey(key)) variables[key] += delta;
            else variables.Add(key, delta);
            _needsSystem.UpdateByName(key, variables[key]);
        }


        public void OnDialogueStart(string dialogueName, TalkativeObject o)
        {
            InputManager.DialogueMode = true;
            _talkativeObject = o;
            DialoguesManager.Popup.StartDialogue(dialogueName, this);
        }


        public void OnDialogueFinish()
        {
            StatsController.DialoguesCount += 1;
            _talkativeObject.OnFinish();
            InputManager.DialogueMode = false;
        }
    }
}