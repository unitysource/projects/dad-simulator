﻿using UnityEngine;
using UnityEngine.UI;

namespace Mechanics.Player
{
    public class StatsController : MonoBehaviour
    {
        [SerializeField] private Text interactionsCountText;
        [SerializeField] private Text dialoguesCountText;

        private static StatsController _instance;
        private static int _interactionsCount;
        private static int _dialoguesCount;

        private void Start()
        {
            _instance = this;
        }


        public static int InteractionsCount
        {
            get => _interactionsCount;
            set
            {
                _instance.interactionsCountText.text = $"#{value}";
                _interactionsCount = value;
            }
        }

        public static int DialoguesCount
        {
            get => _dialoguesCount;
            set
            {
                _instance.dialoguesCountText.text = $"#{value}";
                _dialoguesCount = value;
            }
        }
    }
}