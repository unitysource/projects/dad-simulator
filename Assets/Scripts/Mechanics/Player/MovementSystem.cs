using UnityEngine;

namespace Mechanics.Player
{
    [RequireComponent(typeof(CharacterController))]
    public class MovementSystem : MonoBehaviour
    {
        [SerializeField] private float speed = 3f;

        private AudioSource _audioSrc;
        public AudioSource AudioSrc => _audioSrc;
        private bool _isMoving;

        private void Start()
        {
            _audioSrc = GetComponent<AudioSource>();
        }

        public void Move(Vector3 pos)
        {
            if (!_audioSrc.isPlaying)
                _audioSrc.Play();
            
            float distance = Vector3.Distance(transform.position, pos);
            Vector3 position = transform.position;
            position = Vector3.Lerp(position, pos, speed * Time.deltaTime / distance);
            transform.position = position;
        }

        public void Flip(Vector3 target)
        {
            Vector3 position = transform.position;
            Vector3 scale = transform.localScale;
            if (target.x < position.x && scale.x < 0 || target.x > position.x && scale.x > 0)
                // transform.Rotate(new Vector3(0, 180, 0));
                transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
        }
    }
}