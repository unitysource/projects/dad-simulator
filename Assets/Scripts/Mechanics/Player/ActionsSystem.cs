﻿using System;
using Mechanics.Environment;
using UnityEngine;

namespace Mechanics.Player
{
    [RequireComponent(typeof(Animator))]
    public class ActionsSystem : MonoBehaviour
    {
        [SerializeField] private float throwForce = 10f;
        [SerializeField] private Transform objectsHolder;
        [SerializeField] private float yMin = 0.5f;

        public Animator PlayerAnimator { get; private set; }

        private void OnEnable()
        {
            InputManager.Throw += ThrowAction;
        }

        private void OnDisable()
        {
            InputManager.Throw -= ThrowAction;
        }

        private void Start()
        {
            PlayerAnimator = GetComponent<Animator>();
        }

        private void ThrowAction(Rigidbody rb, int sign)
        {
            // Debug.Log($"Throw: {rb}");
            Vector3 direction = new Vector3(sign, yMin, 0);
            rb.gameObject.SetActive(true);
            rb.transform.SetParent(objectsHolder);
            rb.AddForce(direction * throwForce);
        }
    }
}