﻿using Mechanics.Environment;
using Mechanics.Environment.ObjectTypes;
using UnityEngine;

namespace Mechanics.Player
{
    public class InventorySystem : MonoBehaviour
    {
        [SerializeField] private Transform inventoryTransform;
        public Rigidbody ObjectRb { get; set; }

        public bool AddItem(PortableObject item)
        {
            if (ObjectRb != null) return false;
            
            Debug.Log(item.transform.parent.name);
            ObjectRb = item.rb;
            item.Parent.transform.SetParent(inventoryTransform);
            item.Parent.transform.localPosition = Vector3.zero;
            item.Parent.SetActive(false);
            return true;
        }
    }
}