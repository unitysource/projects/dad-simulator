﻿using Core.Constrains;
using UnityEngine;

namespace Mechanics.Player
{
    public class CrazyModeSystem : MonoBehaviour
    {
        private NeedStage _stage;

        public void StageUp()
        {
            _stage++;
        }

        public void StageDown()
        {
            _stage--;
        }
    }
}