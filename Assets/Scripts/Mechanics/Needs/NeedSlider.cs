﻿using UnityEngine;
using UnityEngine.UI;

namespace Mechanics.Needs
{
    public class NeedSlider : MonoBehaviour
    {
        [SerializeField] private Image fillImage;
        [SerializeField] private AnimationCurve intensity;
        [SerializeField] [Space] private Color color100;
        [SerializeField] private Color colorOk;
        [SerializeField] private Color colorLess50;
        [SerializeField] private Color colorLess25;
        [SerializeField] private Color colorLess10;


        public void SetSlider(float value)
        {
            fillImage.fillAmount = value * intensity.Evaluate(value);

            if (value > 0.95f) fillImage.color = color100;
            else if (value > 0.5f) fillImage.color = colorOk;
            else if (value > 0.25f) fillImage.color = colorLess50;
            else if (value > 0.10f) fillImage.color = colorLess25;
            else fillImage.color = colorLess10;
        }
    }
}