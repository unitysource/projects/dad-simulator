﻿using System;
using Core.Utilities;
using UnityEngine;

namespace Mechanics.Needs
{
    [Serializable]
    public class Needing
    {
        [SerializeField] private NeedSlider slider;
        [SerializeField] private string name;
        [SerializeField] private short decrement;
        [SerializeField] private short value;

        public NeedSlider Slider => slider;
        public string Name => name;

        public bool Crazy { get; private set; }

        public short Value
        {
            get => value;
            set
            {
                this.value = (short) Mathf.Clamp(this.value + value, 0, 100);
                
                if (!Crazy && this.value < 0.15f)
                    Crazy = RandMe.TryLuck(0.5f);

                if (Crazy && this.value > 0.666f)
                    Crazy = RandMe.TryLuck(0.2f);

                if (Crazy && this.value > 0.9f)
                    Crazy = RandMe.TryLuck(0.666f);
                
                Slider.SetSlider(RelativeValue);
            }
        }

        public float RelativeValue => value / 100f;

        public void Update()
        {
            value -= decrement;
            Slider.SetSlider(RelativeValue);
        }
    }
}