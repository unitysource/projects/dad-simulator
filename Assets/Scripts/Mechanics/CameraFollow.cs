﻿using System;
using Core.Constrains;
using UnityEngine;

namespace Mechanics
{
    public class CameraFollow : MonoBehaviour
    {
        private Transform _target;
        private Vector3 _startOffset;
        [SerializeField] private float speed = .1f;
        [SerializeField] private float floorHeight = 10f;

        public static int FloorNumber { get; set; } = 1;

        private void Start()
        {
            _target = GameObject.FindWithTag(Tags.Player).transform;
            _startOffset = transform.position;
            FloorNumber = 1;
        }

        private void FixedUpdate()
        {
            float floor = FloorNumber == 1 ? 0 : floorHeight;
            Vector3 position = _target.transform.position;
            
            transform.position = Vector3.Lerp(transform.position,
                new Vector3(position.x, _startOffset.y - floor, _startOffset.z),
                speed * Time.deltaTime);
        }
    }
}