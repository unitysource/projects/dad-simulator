﻿using Core.Constrains;
using Core.Dialogues;
using Core.LocalizationTool;
using UI.Popups;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    [SerializeField] private Localization localization;
    [SerializeField] private DialoguesManager dialoguesManager;
    [SerializeField] private DialoguePopup dialoguePopup;
    [SerializeField] private AudioClip defaultSound;
    [SerializeField] private AudioSource interactionSource;

    public static AudioClip DefaultAudio;
    public static AudioSource InteractionSource;
    
    private void Awake()
    {
        this.localization.Init();
        Localization.SetLanguage(PlayerPrefs.GetString(PrefKeys.Language));
        interactionSource.volume = PlayerPrefs.GetFloat(PrefKeys.Sound, 1);
        
        this.dialoguesManager.Init(dialoguePopup);

        DefaultAudio = defaultSound;
        InteractionSource = interactionSource;
    }
}