﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Ui
{
    public class MenuUi : MonoBehaviour
    {
        public void OpenMenu()
        {
            SceneManager.LoadScene(sceneBuildIndex: 0);
        }
        
        public void StartGame()
        {
            SceneManager.LoadScene(sceneBuildIndex: 1);
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}