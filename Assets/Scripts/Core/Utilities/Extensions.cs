﻿using UnityEngine;

namespace Core.Utilities
{
    public static class Extensions
    {
        public static void SetX(ref this Vector3 vector, float newX) => vector.Set(newX, vector.y, vector.z);
        public static void SetY(ref this Vector3 vector, float newY) => vector.Set(vector.x, newY, vector.z);
        public static void SetZ(ref this Vector3 vector, float newZ) => vector.Set(vector.x, vector.y, newZ);
    }
}