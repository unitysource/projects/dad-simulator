﻿using System.Collections.Generic;

namespace Core.LocalizationTool
{
    public static class Extensions
    {
        public static string ToLocalized(this string s)
        {
            return Localization.For(s);
        }

        public static string ToLocalized(this IReadOnlyDictionary<string, string> dict)
        {
            return dict[Localization.CurrentLanguage];
        }
    }
}