﻿using System.Collections.Generic;
using UI.Popups;

namespace Core.Dialogues
{
    public class Dialogue
    {
        private readonly IEnumerator<Replica> replicas;
        private readonly Choice finalChoice;

        public IEnumerator<Replica> Replicas => replicas;
        public Choice FinalChoice => finalChoice;

        public Dialogue(IEnumerable<Replica> replicas, Choice finalChoice)
        {
            this.replicas = replicas.GetEnumerator();
            this.finalChoice = finalChoice;
        }
    }
}