﻿using System;
using Core.Dialogues.Serialization;
using Core.LocalizationTool;
using Mechanics.Player;
using UnityEngine;

namespace Core.Dialogues
{
    [Serializable]
    public class ChoiceOption
    {
        public string Text { get; set; }
        public string Influence { get; set; }

        public Dialogue NextDialogue { get; set; }

        public ChoiceOption(LocalizedChoiceOption localizedChoiceOption, DialogueSystem dialogueSystem)
        {
            this.Influence = localizedChoiceOption.Influence;
            this.Text = localizedChoiceOption.Text.ToLocalized();

            if (localizedChoiceOption.NextDialogue != null)
                this.NextDialogue = DialoguesManager.LoadDialogue(localizedChoiceOption.NextDialogue, dialogueSystem);
        }

        public void Apply(DialogueSystem dialogueSystem)
        {
            if (string.IsNullOrEmpty(Influence)) return;
            
            foreach (string influenceExpression in Influence.Split(';'))
            {
                string[] parts = influenceExpression.Split('=');
                string variable = parts[0];
                int delta = int.Parse(parts[1].Substring(1, parts[1].Length - 1));

                delta = parts[1][0] switch
                {
                    '+' => delta,
                    '-' => -delta,
                    '=' => delta - dialogueSystem.GetVariable(variable),
                    _ => delta
                };

                dialogueSystem.UpdateVariable(variable, (short) delta);
            }
        }
    }
}