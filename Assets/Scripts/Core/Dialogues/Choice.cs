﻿using System.Collections.Generic;
using System.Linq;
using Core.Dialogues.Serialization;
using Mechanics.Player;

namespace Core.Dialogues
{
    public class Choice
    {
        public ChoiceOption[] Options { get; set; }

        public Choice(IEnumerable<LocalizedChoiceOption> choiceOptions, DialogueSystem dialogueSystem)
        {
            this.Options = choiceOptions
                .Where(option => IsVisible(option, dialogueSystem))
                .Select(lco => new ChoiceOption(lco, dialogueSystem)).ToArray();
        }

        private static bool IsVisible(LocalizedChoiceOption option, DialogueSystem dialogueSystem)
        {
            string condition = option.Condition;

            if (string.IsNullOrWhiteSpace(condition))
                return true;

            bool andResult = false;

            foreach (string conditionExpression in condition.Split('&'))
            {
                bool orResult = false;

                foreach (string expression in conditionExpression.Split('|'))
                {
                    if (condition.Contains('>'))
                    {
                        string[] parts = expression.Split('>');
                        string variable = parts[0];
                        int value = int.Parse(parts[1]);
                        int currentValue = dialogueSystem.GetVariable(variable);
                        orResult = value > currentValue;
                    }

                    if (condition.Contains('<'))
                    {
                        string[] parts = expression.Split('<');
                        string variable = parts[0];
                        int value = int.Parse(parts[1]);
                        int currentValue = dialogueSystem.GetVariable(variable);
                        orResult = value < currentValue;
                    }

                    if (condition.Contains('='))
                    {
                        string[] parts = expression.Split('=');
                        string variable = parts[0];
                        int value = int.Parse(parts[1]);
                        int currentValue = dialogueSystem.GetVariable(variable);
                        orResult = value == currentValue;
                    }

                    if (orResult is true) break;
                }

                andResult = orResult;
                if (andResult is false) break;
            }

            return andResult;
        }
    }
}