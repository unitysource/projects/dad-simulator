﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Dialogues.Serialization;
using Mechanics.Player;
using Newtonsoft.Json;
using UI.Popups;
using UnityEngine;

namespace Core.Dialogues
{
    [CreateAssetMenu(fileName = "DialoguesManager", menuName = "Dialogues/Dialogues Manager", order = 1)]
    public class DialoguesManager : ScriptableObject
    {
        [SerializeField] private string dialogueBaseDirPath;
        [SerializeField] [Space] private List<Speaker> speakers;

        private static string _dialogueDirPath;
        private static IReadOnlyDictionary<string, Speaker> _speakersDict;
        
        public static DialoguePopup Popup { get; private set; }

        public void Init(DialoguePopup dialoguePopup)
        {
            _dialogueDirPath = dialogueBaseDirPath;
            _speakersDict = speakers.ToDictionary(s => s.LocalizedName);
            Popup = dialoguePopup;
        }


        public static Dialogue LoadDialogue(string name, DialogueSystem dialogueSystem)
        {
            // Debug.Log($"Dialogue: {Path.Combine(_dialogueDirPath, name)}");
            
            if (string.IsNullOrEmpty(name))
            {
                Debug.LogWarning("Dialogue name is not defined.");
                return null;
            }

            Debug.Log(Path.Combine(_dialogueDirPath, name));
            var jsonAsset = Resources.Load<TextAsset>(Path.Combine(_dialogueDirPath, name));
            var dialogueSource = JsonConvert.DeserializeObject<LocalizedDialogue>(jsonAsset.text);
            IEnumerable<Replica> replicas = dialogueSource!.Replicas.Select(r => new Replica(r));
            var dialogue = new Dialogue(replicas, new Choice(dialogueSource.ChoiceOptions, dialogueSystem));
            return dialogue;
        }

        public static Speaker GetSpeakerByName(string name) => _speakersDict[name];
    }
}