﻿using Core.Dialogues.Serialization;
using Core.LocalizationTool;

namespace Core.Dialogues
{
    public class Replica
    {
        public Speaker Speaker { get; set; }
        public string Text { get; set; }

        public Replica(LocalizedReplica localizedReplica)
        {
            this.Speaker = DialoguesManager.GetSpeakerByName(localizedReplica.Speaker);
            this.Text = localizedReplica.Text.ToLocalized();
        }
    }
}