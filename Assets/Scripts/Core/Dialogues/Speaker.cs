﻿using System;
using UnityEngine;

namespace Core.Dialogues
{
    [Serializable]
    public struct Speaker
    {
        [SerializeField] private Sprite avatar;
        [SerializeField] private string nameLocalizationCode;
        [SerializeField] private Color color;

        public Sprite Avatar => avatar;
        public string LocalizedName => nameLocalizationCode;
        public Color Color => color;
    }
}