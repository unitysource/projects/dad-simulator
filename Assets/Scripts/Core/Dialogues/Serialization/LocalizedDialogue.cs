﻿using System;

namespace Core.Dialogues.Serialization
{
    [Serializable]
    public class LocalizedDialogue
    {
        public LocalizedReplica[] Replicas { get; set; }
        public LocalizedChoiceOption[] ChoiceOptions { get; set; }

    }
}