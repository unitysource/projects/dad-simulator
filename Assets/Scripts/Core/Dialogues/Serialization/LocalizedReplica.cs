﻿using System;
using System.Collections.Generic;

namespace Core.Dialogues.Serialization
{
    [Serializable]
    public class LocalizedReplica
    {
        public string Speaker { get; set; }
        public Dictionary<string, string> Text { get; set; }
    }
}