﻿using System;
using System.Collections.Generic;

namespace Core.Dialogues.Serialization
{
    [Serializable]
    public class LocalizedChoiceOption
    {
        public string NextDialogue { get; set; }
        public string Condition { get; set; }
        public string Influence { get; set; }
        public IReadOnlyDictionary<string, string> Text { get; set; }
    }
}