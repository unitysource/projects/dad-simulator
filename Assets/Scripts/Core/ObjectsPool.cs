﻿using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class ObjectsPool : MonoBehaviour
    {
        [SerializeField] private int countOfStackElements = 3;
        [SerializeField] private GameObject toolTipModel;
        
        private static readonly Queue<GameObject> TipsPool = new Queue<GameObject>();

        private static ObjectsPool _instance;
        private void Start()
        {
            _instance = this;
            
            for (int i = 0; i < countOfStackElements; i++)
            {
                GameObject instance = Instantiate(toolTipModel, transform);
                StoreToolTip(instance);
            }
        }

        public static void StoreToolTip(GameObject obj)
        {
            obj.transform.SetParent(_instance.transform);
            obj.SetActive(false);
            TipsPool.Enqueue(obj);
        }

        public static GameObject ActivateToolTip()
        {
            if (TipsPool.Count > 0)
            {
                return TipsPool.Dequeue();
            }

            Debug.Log($"Pool {TipsPool} is empty!");
            GameObject instance = Instantiate(_instance.toolTipModel, _instance.transform);
            return instance;
        }
    }
}