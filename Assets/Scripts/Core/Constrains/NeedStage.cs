﻿using System;

namespace Core.Constrains
{
    [Serializable]
    public enum NeedStage : byte
    {
        Nutrition,
        ThirstOld,
        Grumble,
        Pills,
    }
}